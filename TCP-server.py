import socket

HOST = ''  # Standard loopback interface address (localhost)
PORT = 65432    	# Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s: # AF_INET is the Internet address family for IPv4, SOCK_STREAM is the socket type for TCP
    s.bind((HOST, PORT)) # associate the HOST with the PORT
    s.listen()
    conn, addr = s.accept() #create a new socket
    with conn: #with permits to avoid errors
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            conn.sendall(data)