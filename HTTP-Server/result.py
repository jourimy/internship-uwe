#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 19:57:04 2020

@author: root
"""

import cgi  #The manager which executes the scripts
import cgitb #To deal with forms
import urllib.request #To import an URL
import json #To encode and decode JSON

cgitb.enable() #To debug
form = cgi.FieldStorage() #To collect the informations

#DATABASE
#Normal authentication
user1="Juan"
password1="MLKO0"

user2="Stephen"
password2="OAML0"
#Authentication via email
pass1 = form.getvalue("passtoverify")

#Import the informations about the user
#Localisation
with urllib.request.urlopen("https://geolocation-db.com/json/6db070f0-7c27-11ea-8264-e974339fc182") as url: #Importing informations about the user
    data = json.loads(url.read().decode()) #Transform JSON into dict
    country_code = data['country_code']#keep only what's needed 
    country_name = data['country_name'] 
    city = data['city'] 
    postal = data['postal']
    latitude = data['latitude']
    longitude = data['longitude']
    IPv4 = data['IPv4']

    
    

#HTML
print("Content-type: text/html; charset=utf-8\n") #To indicate that CGI have to execute this script in html

html0 = """<!DOCTYPE html>

<HEAD>

    <TITLE>Result HTTP page </TITLE>
    
    <script>
        function info(){
                var navinfo = navigator.appCodeName + "/" + navigator.appVersion + "/" + navigator.language; 
                var platinfo = navigator.platform
                var today = new Date();
            
                document.getElementById("tdnavinfo").innerHTML = navinfo; 
                document.getElementById("tdplatinfo").innerHTML = platinfo;
    
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                document.getElementById("tdtime").innerHTML = time;
                var t = setTimeout(info,500); 
                }
    </script> <! -- To gather informations about the system and the browser, calling every half second the function -->

</HEAD>

<BODY onload="info()">
    <style>
        body {
            background: linear-gradient(to left, DarkCyan, CornflowerBlue); /* To display background color fade */
            }
        .style1 {
                font-family: "Lucida Console", Courier, monospace;
                }
        #A {
            position: absolute;
            bottom: 0;
            right: 0;
            cursor: crosshair;
            }
        #divDisplay {
                     font-family: "Courier New", Courier, monospace;
                    }
        #divDisplay:hover {
                           width: 300px;
                          }
    </style>
    
    
"""


html1 = """
    </br></br></br>
    <CENTER><div>  
        <table cellpadding="2" cellspacing="2" style=" border:1px solid #000; font-family:'Segoe UI';" >  
            <tr style="background-color:#334785; color:White; font-weight:bold">  
                <td colspan="2" align="center">Context about you</td>  
            </tr>  
            <tr style="border:solid 1px #000000">  
                <td align="left"><b>Country code:</b></td>  
                <td>"""


html2 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>Country  :</b></td>  
                <td>"""


html3 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>City  :</b></td>  
                <td>"""
                

html4 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>Postal  :</b></td>  
                <td>"""
                

html5 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>Latitude  :</b></td>  
                <td>"""
                

html6 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>Longitude  :</b></td>  
                <td>"""
                

html7 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>IPv4  :</b></td>  
                <td>"""

html8 = """
                </td>  
            </tr>  
            <tr>  
                <td align="left"><b>Browser  :</b></td>  
                <td id="tdnavinfo"></td> 
            </tr>
            <tr>  
                <td align="left"><b>Platform  :</b></td>  
                <td id="tdplatinfo"></td> 
            </tr>
            <tr>  
                <td align="left"><b>Time  :</b></td>  
                <td id="tdtime"></td> 
            </tr>
        </table>  
    </CENTER></div>
    <CENTER>
        <div id="divDisplay" >                                        
            <form method="post" action="company_secret.py">              <! -- To send the informations to the file secret_company.py with post which hide content --> 
                <p>
                    </br><hr></br>Enter the secret password :</br><input type="password" name="secret"></br>
                    <input type="submit" value="Send">
                </p>
            </form>
        </div>
        
        <script>
            var divDisplay = document.getElementById("divDisplay")
            divDisplay.style.display = 'none' 
            
            function Display() { 
                    divDisplay.style.display = 'block'      
                    var A = document.getElementById("A")
                    A.style.display = 'none';
                    }
        </script>                                                <! -- To hide the div with the ID --> 
        
        <button id="A" type="button" onclick="Display()">Only for headmasters of the company</button>   <! -- When we click on it, it calls the function underneath -->
 
    </CENTER>
</BODY>
    
</html>

"""




#Principal code
try:
    if form.getvalue("username")==user1 and form.getvalue("password")==password1:   #For a normal authentication
        username = form.getvalue("username")
        print("<CENTER>Hello", username, "</CENTER>")
        print(html0,html1,country_code,html2,country_name,html3,city,html4,postal,html5,latitude,html6,longitude,html7,IPv4,html8)
        print("<script>alert('You are our first user!')</script>")
    elif form.getvalue("username")==user2 and form.getvalue("password")==password2: #For a normal authentication
        username = form.getvalue("username")
        print("<CENTER>Hello", username, "</CENTER>")
        print(html0,html1,country_code,html2,country_name,html3,city,html4,postal,html5,latitude,html6,longitude,html7,IPv4,html8)
        print("<script>alert('You are our second user!')</script>")
    elif form.getvalue("pass")!=None and form.getvalue("pass")==pass1:                                              #For an authentication via email 
        print("<CENTER>Hello dear user</CENTER>")
        print(html0,html1,country_code,html2,country_name,html3,city,html4,postal,html5,latitude,html6,longitude,html7,IPv4,html8)
        print("<script>alert('You have been connected via your email!')</script>")
    else:
        raise Exception("Username or password may be wrong")
except:
    print("Username or password may be wrong")
    
