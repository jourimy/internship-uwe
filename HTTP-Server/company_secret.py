#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 19:56:44 2020

@author: root
"""

import cgi  #The manager which executes the scripts
import cgitb #To deal with forms

cgitb.enable() #To debug
form = cgi.FieldStorage() #To collect the informations

#DATABASE
secret='NBAZ7832§§'

#html
print("Content-type: text/html; charset=utf-8\n") #To indicate that CGI have to execute this script in html


html = """<!DOCTYPE html>


<HEAD>

    <TITLE>Simple HTTP page </TITLE>

</HEAD>

<BODY  >
    
    <style>
        
        body {
              background: linear-gradient(to right, LightSeaGreen, LightSkyBlue); /* To display background color fade */
              font-family: Avantgarde, TeX Gyre Adventor, URW Gothic L, sans-serif;
             }
        #double {
                border-style: double;
                border-width:thick;
                width : 30%;
                }
    </style>

    <CENTER>"""

print(html)

html1="""
        <p>Here you can see the private informations about JGB Ltd:</p> </br> </br> </br> </br>
        
        <div id="double" >
        <h3>JGB Ltd</h3>
        ‡ Locality : Toulouse</br>
        ‡ Principal email address : Jgb_Ltd@gmail.com</br>
        ‡ Numbers of employees : 50</br>
        ‡ Turnover : 40 000 000 $</br>
        ‡ Leader : Jeremy Grondin</br>
        ‡ Managers : Juan, Stephen</br>
        ‡ Projects done : 47</br>
        ‡ Unknown collaborators : 20</br>
        </br>
        </br>
        ◊ Secret project in progress : Robotracker
        </div>
        
    </CENTER>
</BODY>

</html>
"""

#Principal code
try:
    if form.getvalue("secret")==secret:
        print(html1)
    else:
        raise Exception("Secret password may be wrong")
except:
    print("Secret password may be wrong")
