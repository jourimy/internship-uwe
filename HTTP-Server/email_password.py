#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 21:45:46 2020

@author: root
"""

import cgi
import cgitb #To deal with forms

cgitb.enable() #To debug
form = cgi.FieldStorage() #To collect the informations



print("Content-type: text/html; charset=utf-8\n") #To indicate that CGI have to execute this script in html


html = """<!DOCTYPE html>


<HEAD>

    <TITLE>Simple HTTP page </TITLE>

</HEAD>

<BODY  >
    
    <style>
        body {
            background: linear-gradient(to right, DarkSlateBlue, DeepSkyBlue); /* To display background color fade */
            }
        .style1 {
                font-family: "Lucida Console", Courier, monospace;
                }
    </style>

    <CENTER>

        <H1>We are on a HTTP Server coded in python</H1>
        
        
        <div id="divDisplay2">                                        
            <form method="post" action="result.py">              <! -- To send the informations to the file result.py --> 
            <p class="style1">
            Check your emails to find the password:
            </p>
            <p>
            Password:<input type="password" name="pass"></br>
            <input type='hidden' name='passtoverify' value="""
            
html1=      """>
            <input type="submit" value="Send">
            </p>
        </div>
        
    </CENTER>

</BODY>

</html>
"""

#To create a random password with digits and numbers
import random
import string

def get_random_alphaNumeric_string(stringLength):
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join((random.choice(lettersAndDigits) for i in range(stringLength)))

#DATABASE
email1 = form.getvalue("email")
pass1 = get_random_alphaNumeric_string(8)

#Sending password by email 
from email.mime.multipart import MIMEMultipart #To create an email
from email.mime.text import MIMEText #To create a text for emails
import smtplib #To use smpt, protocol for mails

msg = MIMEMultipart() #To create a new email
msg['From'] =  "example@gmail.com"
msg['To'] = email1
password =  "pasword" #password of the forwarder 
msg['Subject'] = "Informations for your connexion"
body = "Your password is " + pass1 #The message
msg.attach(MIMEText(body, 'html'))

server = smtplib.SMTP("smtp.gmail.com", 587) #To provide informations about the Google's SMTP server 
server.starttls()# To connect
server.login(msg['From'],password)#To connect with the forwarder's address
server.sendmail(msg['From'],msg['To'],msg.as_string())
server.quit()




print(html,pass1,html1, "</br><CENTER> Your email is ", email1, "</CENTER>")
