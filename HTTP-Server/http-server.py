#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 15:44:25 2020

@author: root
"""

import http.server 
import ssl



port = 443 
address = ("", port)

server = http.server.HTTPServer #To create the server

handler = http.server.CGIHTTPRequestHandler #CGI is the manager which will execute scripts
handler.cgi_directories = ["/"]

httpd = server(address, handler) #To create the deamon which will runned with our address
httpd.socket = ssl.wrap_socket(httpd.socket, certfile='./server.pem', server_side=True) # To encrypt the traffic with the file generated, the SSL certificate and keyw


print("Server launched on port : ", port)

http.server.CGIHTTPRequestHandler.have_fork=False # Force the use of a subprocess  

httpd.serve_forever() #To launch the server 
