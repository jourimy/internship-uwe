#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 19:35:54 2020

@author: root
"""

import cgi

print("Content-type: text/html; charset=utf-8\n") #To indicate that CGI have to execute this script in html


html = """<!DOCTYPE html>


<HEAD>

    <TITLE>Simple HTTP page </TITLE>

    <p id="txt"></p>
    
    <script>
        function askpermission() {
          var txt;
          if (confirm("This website will access to your position, your browser and platform's information only to display them to you")) {
          } else {
            txt = "You didn't agree to share your informations with us ";
            document.getElementById("A").style.display = 'none';
            document.getElementById("C").style.display = 'none';
            document.getElementById("h1").style.display = 'none';
            document.getElementById("txt").innerHTML = txt;
          }
          }
    </script>
    
</HEAD>

<BODY onload="askpermission()" >
    
    <style>
        body {
            background: linear-gradient(to right, Azure, Aquamarine); /* To display background color fade */
            }
        .style1 {
                font-family: "Lucida Console", Courier, monospace;
                }
    </style>

    <CENTER>

        <H1 id="h1" >We are on a HTTP Server coded in python</H1>
        
        <div id="divDisplay1">                                        
            <form method="post" action="result.py">              <! -- To send the informations to the file result.py with post which hide content --> 
            <p>
            Username:<input type="text" name="username"></br>
            Password:<input type="password" name="password"></br>
            <input type="submit" value="Send">
            </p>
            </form>
        </div>
        
        <div id="divDisplay3">                                        
            <form method="post" action="email_password.py">              <! -- To send the informations to the file email-password.py with post which hide content--> 
            <p class="style1">
            Enter your Gmail address to receive the password:
            </p>
            <p>
            <input type="email" name="email"></br>
            <input type="submit" value="Next step">
            </p>
            </form>
        </div>
        
        <script>
            var divDisplay1 = document.getElementById("divDisplay1")
            divDisplay1.style.display = 'none'
            var divDisplay3 = document.getElementById("divDisplay3")
            divDisplay3.style.display = 'none'  
        </script>                                                <! -- To hide the div with the ID divFormu --> 
        
        <button id="A" type="button" onclick="Display1()">Normal authentication</button>   <! -- When we click on it, it calls the function underneath --> 
        
        <button id="C" type="button" onclick="Display3()">Authentication by email</button>
    
        <script>
            function Display1() { 
                    divDisplay1.style.display = 'block'      
                    var A = document.getElementById("A")
                    A.style.display = 'none';
                    C.style.display = 'none';
                    }
            
            function Display3() { 
                    divDisplay3.style.display = 'block'      
                    var C = document.getElementById("C")
                    A.style.display = 'none';
                    C.style.display = 'none';                    
                    }
        </script>                                                <! -- To display the form and hide the button -->
    
    
        
    </CENTER>

</BODY>

</html>
"""


print(html)